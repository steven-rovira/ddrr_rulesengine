# start sql command 
# 
import os
import psycopg2
import psycopg2.extras
import re
import traceback
from coalesce import coalesce

def orgQuery(orgId, batchNumber, scoreContextId):
    txt = ' and "scoreHistoryMethodResult"."scoreBatchId" is null' if orgId else ''
    return f'''SELECT org.id
, org."VENDOR_SUPPLY_NAME"
, org."QC NAICS 1"
, qcNaics1."naicsName"
, qcNaics1Ind."scoreIndustryId"
, qcnaics1ind."scoreIndustryName"
,org."QC Revenue Annual"
,org."QC Employee Count"
,"QC Offshore Services"
,"QC Cloud Services"
,"QC Mobile Services"
,"QC Offshore Services Comment" 
,"QC Cloud Services Comment"
,"QC Mobile Services Comment"
,is_ot
FROM dbo."ddrr_organization" as org
inner join dbo."ddrr_naicsInherentRiskIndustry" as qcNaics1 on qcNaics1."naicsCode" = org."QC NAICS 1"
inner join dbo."scoreIndustry" as qcNaics1Ind on qcNaics1Ind."scoreIndustryId" = qcNaics1."inherentRiskIndustryId"
left join (select distinct "organizationId", "scoreBatchId" from dbo."scoreHistoryMethodResult" where "scoreBatchId"={batchNumber} and "scoreHistoryMethodResult"."scoreContextId"={scoreContextId} {txt}) as score on score."organizationId"= org.id
where qcNaics1Ind."scoreIndustryId" IS NOT NULL{f" and org.id = {orgId} LIMIT 1" if orgId else ''}'''

def latestCrawlerBatch(_id):
    return f"select \"crawlerBatch\" from dbo.\"ddrr_crawlerResult\" where \"organizationId\" = {_id} order by \"lastCrawlTimeUTC\" desc LIMIT 1"

def scrapeQuery(_id, batch):
    return f"select count(id) as count from dbo.\"ddrr_crawlerResult\" where \"crawlerBatch\"='{batch}' and \"organizationId\"={_id}"
    
def keyword_query(_id, crawlerBatch):
    return f"""--new query for pulling the crawlerResults
    select * from 
    (select * from dbo.\"ddrr_crawlerResult-crawlerKeyword\" where \"organizationId\" = {_id}) as link
       left join dbo.\"ddrr_crawlerKeyword\" as keyword on keyword.id = link.\"crawlerKeywordId\"
       left join (select id
          ,title
          ,\"humanLanguage\"
          ,\"pageUrl\"
          --,[text]
          ,\"docId\"
          ,\"parentUrlDocId\"
          ,\"lastCrawlTimeUTC\"
          ,\"timestamp\"
          ,\"fromSeedUrl\"
          ,\"crawlerBatch\"
          ,\"organizationId\"  
          from dbo.\"ddrr_crawlerResult\" where \"organizationId\" = {_id} and \"crawlerBatch\" = '{crawlerBatch}') 
          as result on result.id = link.\"crawlerResultId\"
     --order by keyword.keyword"""

def ms_escape_string(data):
    if not data:
        return ''
    data = re.sub('/%0[0-8bcef]/','', data)
    data = re.sub('/%1[0-9a-f]/', '', data)
    data = re.sub('/[\x00-\x08]/', '', data)
    data = re.sub('/\x0b/', '', data)
    data = re.sub('/\x0c/', '', data)
    data = re.sub('/[\x0e-\x1f]/', '', data)
    data = re.sub("'", "''", data)
    return data

def run_Rules_Engine(event,context=None):
    batchNumber = 12
    scoreContextId = 1
    orgId = None
    if event['orgId']:
        orgId = event['orgId']
    output = {}
    try:
        connection = psycopg2.connect(user = 'fisDDRR',
                                    password = 'sJkfM^y12^%O',
                                    host = 'ddrr.ce6jjr42f8zr.us-east-1.rds.amazonaws.com',
                                    port = '5432',
                                    database = 'fp_ddrr',)

        
        cursor = connection.cursor(cursor_factory=psycopg2.extras.DictCursor)

        orgSql = orgQuery(orgId, batchNumber, scoreContextId)

        output['organizationSql'] = orgSql

        # Print PostgreSQL version
        cursor.execute(orgSql)

        recordNumber = 1
        records = []
        for r in cursor.fetchall():
            records.append(dict(r))

        # records = cursor.fetchall()
        # print(type(records))
        for record in records:
            # print(record)
            # record = dict(r)
            output[recordNumber] = {}
            output[recordNumber]['message'] = f"{record['VENDOR_SUPPLY_NAME']} {record['id']}\n"
            output[recordNumber]['organizationId'] = record['id']

            crawlerOffshorePageCount = 0 
            crawlerCloudPageCount=0
            crawlerMobilePageCount=0
            textOffshore=""
            textCloud=""
            textMobile=""

            cursor.execute(latestCrawlerBatch(record['id']))
           
            latestBatch = cursor.fetchone()
            crawlerBatch = dict(latestBatch)['crawlerBatch'] if latestBatch else ''

            cursor.execute(scrapeQuery(record['id'], crawlerBatch))
            scrapeCount = dict(cursor.fetchone())
            
            keywQuery = keyword_query(record['id'], crawlerBatch)
            cursor.execute(keywQuery)
            keyWordMatches = []
            for k in cursor.fetchall():
                keyWordMatches.append(dict(k))
            

            for keywordMatch in keyWordMatches:
                if keywordMatch['category'] is None:
                    keywordMatch['category'] = ''

                if keywordMatch['category'].lower() == 'offshore'.lower():
                    crawlerOffshorePageCount+=1
                    textOffshore = textOffshore + f"<span title=\"Keyword: {keywordMatch['keyword']}\"><button onclick=\"window.open('{keywordMatch['pageUrl']}')\">{crawlerOffshorePageCount}</button></span> "
                elif keywordMatch['category'].lower() == 'cloud'.lower():
                    crawlerCloudPageCount += 1
                    textCloud = textCloud + f"<span title=\"Keyword: {keywordMatch['keyword']}\"><button onclick=\"window.open('{keywordMatch['pageUrl']}')\">{crawlerOffshorePageCount}</button></span> "
                elif keywordMatch['category'].lower() == 'mobile'.lower():
                    crawlerMobilePageCount += 1
                    textMobile = textMobile + f"<span title=\"Keyword: {keywordMatch['keyword']}\"><button onclick=\"window.open('{keywordMatch['pageUrl']}')\">{crawlerOffshorePageCount}</button></span> "
            
            crawlerTotalPageCount = scrapeCount['count']

            crawlerOffshorePercentage = 0
            crawlerCloudPercentage = 0
            crawlerMobilePercentage = 0
            if crawlerTotalPageCount != 0:
                crawlerOffshorePercentage = crawlerOffshorePageCount / crawlerTotalPageCount
                crawlerCloudPercentage = crawlerCloudPageCount / crawlerTotalPageCount
                crawlerMobilePercentage = crawlerMobilePageCount / crawlerTotalPageCount
            
            updateCrawlerQuery = f"""update dbo.ddrr_organization set
                  \"crawlerTotalPageCount\" = {crawlerTotalPageCount}
                , \"crawlerOffshorePageCount\" = {crawlerOffshorePageCount}
                , \"crawlerOffshorePercentage\" = {crawlerOffshorePercentage}
                , \"crawlerCloudPageCount\" = {crawlerCloudPageCount}
                , \"crawlerCloudPercentage\" = {crawlerCloudPercentage}
                , \"crawlerMobilePageCount\" = {crawlerMobilePageCount}
                , \"crawlerMobilePercentage\" = {crawlerMobilePercentage}
                where id={record['id']} """
            
            cursor.execute(updateCrawlerQuery)
            cursor.execute(f"DELETE FROM dbo.\"scoreHistoryMethodResult\" WHERE \"organizationId\"={record['id']} and \"scoreBatchId\"={batchNumber} and \"scoreContextId\"={scoreContextId}")
            output[recordNumber]['deleteExistingMessage'] = f"deleted company records in scoreMethodResult table where ID = {record['id']}"

            cursor.execute(f"DELETE FROM dbo.\"score_history_method_result_upload\" WHERE \"organizationId\"={record['id']} and \"scoreBatchId\"={batchNumber} and \"scoreContextId\"={scoreContextId}")
            output[recordNumber]['deleteExistingMessage'] = f"deleted company records in score_history_method_result table where ID = {record['id']}"

            # break
            array_answerId = {}
            array_answerValue = {}
            array_answerScore = {}
            array_internalText = {}
            array_externalText = {}
            # array_questionId = {}
            tsql = f"""SELECT 
                ma.\"scoreMethodAnswerId\",
                ma.\"answerValue\",
                ma.\"answerScore\",
                ma.\"scoreMethodId\",
                ma.\"scoreQuestionId\",
                ma.\"scoreContextId\"
                FROM dbo.\"scoreMethodAnswer\" as ma 
                inner join dbo.\"scoreIndustryAnswer\" as ia on ma.\"scoreMethodAnswerId\" = ia.\"scoreMethodAnswerId\"
                inner join dbo.\"scoreIndustry\" as i on i.\"scoreIndustryId\"=ia.\"scoreIndustryId\"
                where i.\"scoreIndustryId\"={str(record['scoreIndustryId'])} and ma.\"scoreContextId\" = {str(scoreContextId)}
                ORDER BY ma.\"scoreQuestionId\""""

            cursor.execute(tsql)

            manualHighRisk = 0 
            fourthPartyRisk = 0

            answers = []
            for a in cursor.fetchall():
                answers.append(dict(a))

            for answer in answers:
                array_answerId[answer['scoreMethodId']] = answer['scoreMethodAnswerId']
                array_answerValue[answer['scoreMethodId']] = answer['answerValue']
                array_answerScore[answer['scoreMethodId']] = answer['answerScore']
                array_internalText[answer['scoreMethodId']] = f"Answer is based entirely on the industry classification of {record['scoreIndustryName']}"
                array_externalText[answer['scoreMethodId']] = "Third-party databases, machine learning and manual QC suggest this."

                rsql = f"""SELECT *  FROM dbo.\"ddrr_manualReviewResult\" AS m       
                            WHERE m.\"organizationId\" = {record['id']}
                            AND m.\"scoreValue\" = 1500
                            """

                cursor.execute(rsql)
                manualRow = len(cursor.fetchall())

                if (manualRow > 0 ):
                    manualHighRisk = 1

        
            fpsql= f"""   SELECT \"QC NAICS 1\", id
                        FROM dbo.ddrr_organization as o
                        INNER JOIN dbo.\"ddrr_naicsInherentRiskIndustry\" AS n
                        ON o.\"QC NAICS 1\" = n.\"naicsCode\"
                        WHERE ( ((n.\"inherentRiskIndustryId\" = 2 --information processing
                        OR n.\"inherentRiskIndustryId\" = 9)  --computer programming
                        and \"QC Revenue Annual\" > 100000000) -- gt 100Mn
                        AND \"QC Cloud Services\" = 'Yes')
                        AND id = {record['id']}
                    """

            cursor.execute(fpsql)

            fpRow = len(cursor.fetchall())

            if fpRow > 0:
                fourthPartyRisk = 1
            

            tsql = f"""SELECT * FROM dbo.\"scoreMethod\" as m 
                left join dbo.\"scoreQuestion\" as q on q.\"questionId\" = m.\"questionId\"
                left join dbo.\"scoreCategory\" as c on c.\"categoryId\" = q.\"questionCategoryId\"
                where \"methodMaxScore\"<>0 and m.\"scoreContextId\" = {str(scoreContextId)}
                order by \"questionMethodId\" desc -- this is important so that record id 20 (scrape) will come before record 19 (Overseer) b/c overseer has a dependency on scrape """
            
            cursor.execute(tsql)
        
            # scoreMethod_index = 1
            scoreMethods = []
            for s in cursor.fetchall():
                scoreMethods.append(dict(s))
            
            for scoreMethod in scoreMethods:
                output[recordNumber]['message'] = "Moving through questionMethods: " + str(scoreMethod['questionMethodId']) + " " + scoreMethod['methodType'] + " " + scoreMethod['questionTitle'] + "\n"

                answerId = 0
                answerValue = ""
                answerScore = 0.0
                internalText = ""
                externalText = ""
                
                if (scoreMethod['questionId'] in {1,6,10,5,2,7}) and record['QC NAICS 1'] == 541620 :
      
                    answerId = array_answerId[scoreMethod['questionMethodId']]
                    answerId = array_answerId[scoreMethod['questionMethodId']]
                    answerValue = 'Special Handling'
                    answerScore = scoreMethod['methodMaxScore']
                    internalText = 'Answer is based entirely on the vendor service classification of Environmental Consulting.'
                    externalText ='Third-party databases, machine learning and manual QC suggest this.'
                elif scoreMethod['methodType'] == 'Industry':
                    tsql = f"""SELECT * FROM dbo.\"scoreMethodAnswer\" as ma 
                        left join dbo.\"scoreIndustryAnswer\" as ia on ma.\"scoreMethodAnswerId\" = ia.\"scoreMethodAnswerId\"
                        left join dbo.\"scoreIndustry\" as i on i.\"scoreIndustryId\"=ia.\"scoreIndustryId\"
                        where ma.\"scoreMethodId\"={scoreMethod['questionMethodId']}  and ma.\"scoreContextId\" = {str(scoreContextId)}
                        and i.\"scoreIndustryId\"={str(record['scoreIndustryId'])}                
                        """
                    answerId = array_answerId[scoreMethod['questionMethodId']]
                    answerValue = array_answerValue[scoreMethod['questionMethodId']]
                    answerScore = min(array_answerScore[scoreMethod['questionMethodId']], scoreMethod['methodMaxScore'])
                    internalText = array_internalText[scoreMethod['questionMethodId']]
                    externalText = array_externalText[scoreMethod['questionMethodId']]
                elif scoreMethod['methodType'] == 'SizeLargerIsRiskier':
                    rev = record['QC Revenue Annual']
                    if rev is None:
                        rev = 0
                    answerValue = "Based on company revenue of " + str(rev)
                    mult = 1
                    if rev > 1000000:
                        mult = 1
                    elif rev > 500000:
                        mult = .67
                    elif rev > 100000:
                        mult = .33

                    answerScore = scoreMethod['methodMaxScore'] * mult
                    internalText = "Answer is based on the size of the company, evidenced by the revenue and number of employees"
                    externalText = "Leveraging public and proprietary databases, the size of this organization can be estimated and is used as an indicator"
                elif scoreMethod['methodType'] == 'SizeSmallerIsRiskier':
                    rev = record['QC Revenue Annual']
                    if rev is None:
                        rev = 0 
                    emp = record['QC Employee Count']
                    if emp is None:
                        emp = 0
                    answerValue = "Based on company revenue of " + str(rev)
                    mult = 1
                    if rev > 1000000 or emp > 1000000/50000:
                        mult = .1
                        text = ">$1mn"
                    elif rev > 500000 or emp > 500000/50000:
                        mult = .33
                        text = ">$500k"
                    elif rev > 100000 or emp > 1000000/50000:
                        mult = .67
                        text = ">$100k"
                    else:
                        mult = 1
                        text = "<1$00k or n/a"
                    
                    answerScore = scoreMethod['methodMaxScore'] * mult
                    internalText = "Company has " + str(rev) + " revenue and " + str(emp) + f" employees which makes it {text}"
                    externalText = "Leveraging public and proprietary databases, the size of this organization can be estimated and is used as an indicator"
                elif scoreMethod['methodType'] == "MTurk":
                    answerScore = 0

                    if scoreMethod['categoryName']=="Offshore":
                        if record['QC Offshore Services']:
                            answerScore = scoreMethod['methodMaxScore']
                            internalText = f"Manual review indicated offshore services are likely. {record['QC Offshore Services Comment']}"
                            externalText = "Manual review indicated possibility of offshore services."
                        else:
                            internalText = f"Manual review did not indicate any observance of offshore services. {record['QC Offshore Services Comment']}"
                            externalText = "Manual review did not give any additional indication of offshore services."

                   
                    if scoreMethod['categoryName']=="Cloud" and scoreMethod['questionId'] != 20:
                        if record['QC Cloud Services']:
                            answerScore = scoreMethod['methodMaxScore']
                            internalText = f"Manual review indicated cloud services are likely.  {record['QC Cloud Services Comment']}"
                            externalText = "Manual reviews indicated likelihood of cloud-based offerings."
                        else:
                            internalText = f"Manual review did not indicate any observance of cloud-based services. {record['QC Cloud Services Comment']}"
                            externalText = "Manual review did not give any additional indication of cloud-based services."

                    if scoreMethod['categoryName']=="Cloud" and scoreMethod['questionId'] == 20:
                        if record['QC Mobile Services']: 
                            answerScore = scoreMethod['methodMaxScore']
                            internalText = f"Manual review indicated mobile services are likely.  {record['QC Mobile Services Comment']}"
                            externalText = "Manual review indicated mobile services may be likely. "
                elif scoreMethod['methodType'] == 'Overseer':
                    tsql = f"""
                    SELECT o.\"countryCode\", cc.\"countryName\", cc.\"isEconomicHardship\", cc.\"isUsEmbargo\", count(o.\"countryCode\") as countryCodeCount
                    FROM dbo.\"ddrr_organizationCountryOverseer\" as o
                    left join dbo.\"scoreCountryCode\" as cc on cc.\"countryCode\" = o.\"countryCode\"
                    where o.\"countryCode\" notnull AND \"organizationId\" = {record['id']} group by o.\"countryCode\", cc.\"countryName\", cc.\"isEconomicHardship\", cc.\"isUsEmbargo\""""

                    cursor.execute(tsql)  
                    offshore = {}
                    offshore['count'] = 0 
                    offshore['text'] = "" 
                    offshore['safeCount'] = 0 
                    offshore['safeText'] = ""

                    i = 0
                    while True:
                        nextRow = cursor.fetchone()
                        if not nextRow:
                            break
                        i += 1
                        overseerResults = dict(nextRow)
                        if overseerResults['isEconomicHardship']==1 or overseerResults['isUsEmbargo']==1:
                            offshore['count'] += 1
                            if overseerResults['isEconomicHardship']==1:
                                hardship='arbitrage'
                            else:
                                hardship=""
                            if overseerResults['isUsEmbargo']==1:
                                embargo='US Embargo'
                            else:
                                embargo=""
                            offshore['text'] += f"<span title=\"{overseerResults['countryName']} {hardship} {embargo}\"><button>{overseerResults['countryCode']}</button></span> "
                        else:
                            offshore['safeCount'] += 1
                            offshore['safeText'] += f"<span title=\"{overseerResults['countryName']}\"><button>{overseerResults['countryCode']}</button></span> "

                    
                    if offshore['count'] == 0:
                        answerScore = 0
                        internalText = "Overseer domain discovery did not return any labor arbitrage or sanctioned countries. "
                        externalText = "Cyber scans did not identify any high-risk offshore locations. "
                    elif offshore['count'] == 1:
                        answerScore = scoreMethod['methodMaxScore'] * .5
                        internalText = "Overseer domain discovery identified an offshore presence. " + offshore['text']
                        externalText = f"Cyber scans found potential offshore activity in one country. {offshore['text']}."
                    else:
                        answerScore = scoreMethod['methodMaxScore']
                        internalText = "Overseer domain discovery identified presence in several labor arbitrage or sanctioned  countries.  " + offshore['text']
                        externalText = "Proprietary cyber scans identified presence in several potentially unsafe locations. " + offshore['text']

                    if offshore['safeCount']>0:
                        append = " Other countries found include " + offshore['safeText']
                        internalText += append
                        externalText += append

                    offshoreOverseerScore = answerScore
                    offshoreOverseerResultCount = i


                    if (offshoreScrapeScore / offshoreScrapeMaxScore) > .5 and (offshoreOverseerScore == 0) and (offshoreOverseerResultCount == 0) and record['QC Offshore Services'] == 'Yes':
                        answerScore = scoreMethod['methodMaxScore']
                        internalText = "Overseer results are not present in the database.  However, manual QC and data scraping both indicate there is an offshore presence thus, an Overseer score has been attributed."
                        externalText = ""
            
                elif scoreMethod['methodType'] == 'Scrape':
                    answerScore = 0

                    if scoreMethod['categoryName']=="Offshore":
                        multiplier = 20
                        confidence = min(crawlerOffshorePercentage * multiplier, 1)
                        answerScore =  confidence * scoreMethod['methodMaxScore']
                        internalText = "crawlerOffshorePageCount of crawlerTotalPageCount web pages scanned had keyword matches for offshoring services.  Multiplier used is multiplier."
                        if textOffshore: 
                            internalText += ("<br>" + textOffshore)
                        
                        externalText = "Based on continuous monitoring of public databases, we have " + str(confidence*100) + "% confidence that offshoring services may be offered."

                        offshoreScrapeScore = answerScore
                        offshoreScrapeMaxScore = scoreMethod['methodMaxScore']

                    if scoreMethod['categoryName']=="Cloud" and scoreMethod['questionId']!= 20:
                        multiplier = 20
                        confidence = min(crawlerCloudPercentage * multiplier, 1)
                        answerScore = confidence * scoreMethod['methodMaxScore']
                        internalText = "crawlerCloudPageCount of crawlerTotalPageCount web pages scanned had keyword matches for cloud services.  Multiplier used is multiplier."
                        if textCloud:
                            internalText += ("<br>" + textCloud)
                        externalText = "Based on continuous monitoring of public databases, we have " + str(confidence*100) + "% confidence that cloud services may be offered."

                    if scoreMethod['categoryName']=="Cloud" and scoreMethod['questionId']==20:
                        multiplier = 20
                        confidence = min(crawlerMobilePercentage * multiplier, 1)

                        if record['QC Mobile Services']:
                            answerScore = confidence * scoreMethod['methodMaxScore']
                            internalText = "crawlerMobilePageCount of crawlerTotalPageCount web pages scanned had keyword matches for mobile services.  Multiplier used is multiplier."
                            if textMobile:
                                internalText += ("<br>" + textMobile)
                            externalText = "Based on continuous monitoring of public databases, we have " + str(confidence*100) + "% confidence that mobile services may be offered."
    
                if scoreMethod['questionCategoryId'] == 1:
                    if manualHighRisk == 1:
                        answerScore = max(answerScore,(scoreMethod['methodMaxScore']*0.8))
                        internalText = f"Answer is based on the industry classification of {record['scoreIndustryName']} and manual review of the company's services offered"
                        externalText = "Third-party databases, machine learning and manual QC suggest this."

                if scoreMethod['questionCategoryId'] == '6':
                    if fourthPartyRisk == 1:
                        answerScore = max(answerScore, (scoreMethod['methodMaxScore'] * 0.8))
                        internalText = f"Answer is based on the industry classification of {record['scoreIndustryName']}, as well as manual review of the company's revenue base and services offered"
                        externalText = "Third-party databases, machine learning and manual QC suggest this."

                if scoreMethod['questionCategoryId'] == '3':
                    if record['is_ot']:
                        answerScore = max(answerScore, (scoreMethod['methodMaxScore'] * 0.8))
                        internalText = f"Answer is based on the industry classification of {record['scoreIndustryName']} and manual review of the company's services offered"
                        externalText = "Third-party databases, machine learning and manual QC suggest this."
                answerScore = round(answerScore)
                tsql = f"""
                --methodType: {scoreMethod['methodType']}
                --categoryName: {scoreMethod['categoryName']}
                INSERT INTO dbo.score_history_method_result_upload
                    (
                    \"organizationId\"
                    ,\"scoreMethodId\"
                    ,\"scoreQuestionId\"
                    ,\"scoreMethodAnswerId\"
                    ,\"scoreBatchId\"
                    ,\"scoreCategoryId\"
                    ,\"scoreContextId\"
                    ,\"history_scoreQuestionDescription\"
                    ,\"history_scoreQuestionTitle\"
                    ,\"history_scoreCategoryName\"
                    ,\"history_scoreQuestionMaxPoint\"
                    ,\"history_questionMethodType\"
                    ,\"history_scoreMethodMaxScore\"
                    ,\"history_scoreMethodAnswerValue\"
                    ,\"history_scoreMethodAnswerScore\"
                    ,\"internalText\"
                    ,\"externalText\"
                    )
                    -- ###############################################################################################################
                VALUES
                    (
                    {record['id']} --organizationId
                    ,{scoreMethod['questionMethodId']} --scoreMethodId
                    ,{scoreMethod['questionId']} --questionId
                    ,{answerId} --scoreMethodAnswerId
                    ,{batchNumber} --scoreBatchId
                    ,{scoreMethod['questionCategoryId']} -- scoreCategoryId
                    ,{scoreContextId} --scoreContextId
                    ,'{ ms_escape_string(scoreMethod['questionDescription'])}' --history_scoreQuestionDescription
                    ,'{ms_escape_string(scoreMethod['questionTitle'])}' --history_scoreQuestionTitle
                    ,'{ms_escape_string(scoreMethod['categoryName'])}' --history_scoreCategoryName
                    ,{scoreMethod['maxPoint']} --history_scoreQuestionMaxPoint
                    ,'{ms_escape_string(scoreMethod['methodType'])}' --history_questionMethodType
                    ,{scoreMethod['methodMaxScore']} --history_scoreMethodMaxScore
                    ,'{ms_escape_string(answerValue)}' --history_scoreMethodAnswerValue
                    ,{answerScore} --history_scoreMethodAnswerScore
                    ,'{ms_escape_string(internalText)}' --internalText
                    ,'{ms_escape_string(externalText)}' --externalText
                    ); 
                """
                # print(tsql)
                # output[recordNumber][scoreMethod['questionMethodId']]['sql'] = tsql
                cursor.execute(tsql)
                # insertScoresForOrganization = dict(cursor.fetchone())
                # print(insertScoresForOrganization)
                # output[recordNumber][scoreMethod['questionMethodId']]['message'] = "id inserted: " + insertScoresForOrganization['organizationId']
            recordNumber += 1
        connection.commit()
        return output
    except (Exception, psycopg2.Error) as error :
        print (error)
        traceback.print_exc()
    finally:
        #closing database connection.
            if(connection):
                cursor.close()
                connection.close()
                print("PostgreSQL connection is closed")
