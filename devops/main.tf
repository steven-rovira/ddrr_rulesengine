provider "aws" {
  region  = "us-west-2"
}

terraform {
  backend "s3" {
    bucket                       = "fortress-platforms"
    key                          = "ddrr/ddrr-lambda"
    region                       = "us-west-2"
  }
}

data "aws_availability_zones" "allzones" {}

data "aws_vpc" "main" {
  id = "vpc-160b0872"
}

data "aws_subnet" "services" {
  id = "subnet-8ceea5fa"
}

data "aws_subnet" "inside" {
  id = "subnet-daeea5ac"
}
##############################Security group
resource "aws_security_group" "ddrr_sec_group" {
  name        = "ddrr_sec_group"
  description = "ddrr_security_group"
  vpc_id      = "${data.aws_vpc.main.id}"

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["${data.aws_subnet.services.cidr_block}","${data.aws_subnet.inside.cidr_block}"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]

  }
}


##################################lambda iam role 
resource "aws_iam_role" "ddrr_lambda" {
    name = "ddrr_lambda"
    assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}
##################################policies for lambda iam role
resource "aws_iam_role_policy_attachment" "AWSLambdaRole" {
    role       = "${aws_iam_role.ddrr_lambda.name}"
    policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaRole"
}

resource "aws_iam_role_policy_attachment" "AWSLambdaVPCAccessExecutionRole" {
    role       = "${aws_iam_role.ddrr_lambda.name}"
    policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole"
}

resource "aws_iam_role_policy_attachment" "CloudWatchLogsFullAccess" {
    role       = "${aws_iam_role.ddrr_lambda.name}"
    policy_arn = "arn:aws:iam::aws:policy/CloudWatchLogsFullAccess"
}


#################################rulesEngine DDRR Lambda
resource "aws_lambda_function" "ddrr_rulesEngine" {
  filename         = "../rulesEngine.zip"
  function_name    = "rulesEngine"
  role             = "${aws_iam_role.ddrr_lambda.arn}"
  handler          = "rulesEngine.run_Rules_Engine"
  #source_code_hash = "${filebase64sha256("../rulesEngine.zip")}"
  runtime          = "python3.7"
  memory_size      = 1408
  timeout          = 10
  vpc_config  {
    subnet_ids  =  ["${data.aws_subnet.services.id}","${data.aws_subnet.inside.id}"]
    security_group_ids = ["${aws_security_group.ddrr_sec_group.id}"]
  }

  environment {
    variables = 
     "${var.rulesEngine_env_vars}"
    
  }
}


#############ApiGW initialize

resource "aws_api_gateway_rest_api" "rulesEngine_api_gateway" {
  name        = "rulesEngine_api_gateway"
  description = "API Gateway for DSN lambda functions"
}

resource "aws_api_gateway_resource" "rulesEngine" {
  rest_api_id = "${aws_api_gateway_rest_api.rulesEngine_api_gateway.id}"
  parent_id   = "${aws_api_gateway_rest_api.rulesEngine_api_gateway.root_resource_id}"
  path_part   = "rulesEngine"
}

########################################################Add Post

#Post
resource "aws_api_gateway_method" "rulesEngine_post" {
  rest_api_id   = "${aws_api_gateway_rest_api.rulesEngine_api_gateway.id}"
  resource_id   = "${aws_api_gateway_resource.rulesEngine.id}"
  http_method   = "POST"
  authorization = "NONE"
  api_key_required ="False"
}

resource "aws_api_gateway_integration" "rulesEngine_api_post" {
  rest_api_id = "${aws_api_gateway_rest_api.rulesEngine_api_gateway.id}"
  resource_id = "${aws_api_gateway_method.rulesEngine_post.resource_id}"
  http_method = "${aws_api_gateway_method.rulesEngine_post.http_method}"

  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = "${aws_lambda_function.ddrr_rulesEngine.invoke_arn}"
}

resource "aws_api_gateway_method_response" "rulesEngine_post_mr" {
  rest_api_id = "${aws_api_gateway_rest_api.rulesEngine_api_gateway.id}"
  resource_id = "${aws_api_gateway_resource.rulesEngine.id}"
  http_method = "${aws_api_gateway_method.rulesEngine_post.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
  }
}

resource "aws_api_gateway_integration_response" "rulesEngine_post_ir" {
  rest_api_id = "${aws_api_gateway_rest_api.rulesEngine_api_gateway.id}"
  resource_id = "${aws_api_gateway_resource.rulesEngine.id}"
  http_method = "${aws_api_gateway_integration.rulesEngine_api_post.http_method}"
  status_code = "${aws_api_gateway_method_response.rulesEngine_post_mr.status_code}"
}

########################################################apigw deployment
resource "aws_api_gateway_deployment" "ddrr_rulesEngine_apigw_deployment" {
  depends_on = [
    "aws_api_gateway_integration.rulesEngine_api_post",
  ]
  rest_api_id = "${aws_api_gateway_rest_api.rulesEngine_api_gateway.id}"
  stage_name  = "rulesEngine"
}

########################################################allow lambda access to gw
resource "aws_lambda_permission" "ddrr_rulesEngine_lambda_perms" {
  statement_id  = "AllowAPIGatewayInvoke"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.ddrr_rulesEngine.arn}"
  principal     = "apigateway.amazonaws.com"

  # The /*/* portion grants access from any method on any resource
  # within the API Gateway "REST API".
  source_arn = "${aws_api_gateway_deployment.ddrr_rulesEngine_apigw_deployment.execution_arn}/*/*"
}

########################################################apigw internal DNS goes here maybe