Necessary python libraries:
fastapi
uvicorn
pydantic

pip install [library]

1) Open a PowerShell in the folder with the rulesEngine.
2) run:
   python app.py
Defaults to localhost:8000
3) Head to http://localhost:8000/docs#/default/runEngine_runRulesEngine_post
4)Click POST next to /runRulesEngine (first div)
5)Click "Try it out" on the rightmost edge
6)In the first textbox, below "Request body", add the ids you want to run in the "ids": array
7)Click Execute