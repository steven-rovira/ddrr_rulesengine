from fastapi import FastAPI
from pathlib import Path
from uvicorn import run
from typing import List
from pydantic import BaseModel
from rulesEngineExelonScrewy import run_Rules_Engine

class Payload(BaseModel):
   ids: List[str]


app = FastAPI()

@app.post("/runRulesEngine")
async def runEngine(payload: Payload):
    return [run_Rules_Engine({ "orgId": o }) for o in payload.ids]

if __name__ == "__main__":
   run(app, host="0.0.0.0", port=8000   , log_level="info")